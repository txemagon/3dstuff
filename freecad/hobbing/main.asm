;
;
; hobbingcontroller.asm
;
; Created: 16/11/2021 18:07:36
; Author : Andy
;



;					LCD 'E'		=PORTB,1 (D9 arduino pin)
;					LCD 'RS'	=PORTB,0 (D8 arduino pin)
;					LCD 'R/W' permanently connected to ground on shield
;					LCD data pins, 4 bit bus
;						D7		=PORTD,7
;						D6		=PORTD,6
;						D5		=PORTD,5
;						D4		=PORTD,4
;					all buttons connected to pin A0(PC0) (various analogue values) (except reset connected to reset)
;
;						stepper controller:
;					STEP=PC1 (A 1)
;					DIR =PC2 (A 1)
;
;					encoder inputs A/B  PD2,3 (INT0/1)  (DIGITAL 2&3)
;
;					LED between PC4(cathode)&PC5 (A4 & A5)
						

.nolist
.include "m328pdef.inc"
.nolist

; Registers
;R0 R1 & R2 used for maths

.DEF	m16h	= R3		;16(+8) bit register for maths
.DEF	m16l	= R4
.DEF	m16f	= R5
.DEF BCDH  = R6 ;	Binary coded decimal result, packed: xxxx3333, 2222,1111
.DEF BCDL  = R7 ;

.DEF encoderl  = R8 ;		encoder low (step on every change on bit 0) running tally
.DEF encoderf  = R9 ;		encoder fraction
.DEF encoderlold  = R10 ;		encoder low last time for comp
.DEF step  = R11			;step buffer 0=no step +=steps forward, -= steps reverse
.DEF maindiv=R12			;MAIN divison (no. teeth) 1-255
.DEF mdtally=R13			;counter for main division
.DEF T2reload=R14			;step delay counter, Max 3c=0.0125s(80 stp/s), max when stopping 0x64 (100 stp/s), Min fe=128us(7800 stp/s)  (64us ticks), 


.DEF rSrg = R15 ; Temporary register for SREG interim storage in ints
.DEF rmp  = R16 ; Universal register outside ints
.DEF rmp2 = R17
.DEF rimp = R18 ; Universal register within ints
.DEF rimp2 = R19  ;
.DEF data = R20	; temporary storage for LCD data


.DEF counter=R22  ;
.DEF flags=R23		;0= 1 keypress flag, 1=update settings (eeprom) flag, 2=step buffer overflow,3 T0 ovf flag,4 moving/stopped 1= moving, 5=step immediately,6=stepped(cleared each t0), 7 increase speed
.DEF flags2=R24		;0=rpm overflow,1=rpm value ready,2,3=reverse motor direction (1=reverse)
;*************************************************************************
;									If the LCD is blank you may need to change E and RS pins
;									Stepper motor starting steps/sec can be adjusted below
.equ	E=1		;(PORTB)
.equ	RS=0	;(PORTB)
.equ	startsteps=0x3c     ;	stepper motor starting speed
;**************************************************************************
.DSEG
.ORG	0x100
keybuf:		.byte 1		;0=no key, 1=right 2=up 3=down 4=left 5=setup
keyin:		.byte 1		;current key code (key held)
keyold:		.byte 1     ;last key code
keydelay:	.byte 1		;
keyval:		.byte 1		;analogue value from switches
keyrepeat:	.byte 1		;repeat delay counter
keyheld:	.byte 1		;key held counter
pos:		.byte 1		;cursor position
ints:		.byte 1		;INT pins copy
oldint1:	.byte 1		;INT value last time
msdbcd:		.byte 1		;limit for max digit
speed:		.byte 2		;stepper speed in steps/s 80-5000 (dec) 0x50-0x1388
acc:		.byte 1		;acceleration delta value calculated from acceleration
rpmmult:	.byte 1		;calculated value to multiply (steps over 16ms period)/4 by to give RPM (14648/x4 encoder cpr)
rpmcount:	.byte 2		;pulse counter for rpm
rpmpre:		.byte 1		;scaled pulse count
rpm:		.byte 2		;current rpm
rpmaverage:	.byte 18	;for averageing
rpmdispctr:	.byte 1		;counter to display rpm on lcd

						;the following values are loaded from eeprom
checksum:		.byte 2
stepsrencoder:	.byte 2		;value 1-9999		*stored as steps with x4 quad encoding, not lines
stepsrspindle:	.byte 2		;value 1-9999
acceleration:	.byte 2		;max stepper acceleration	
maxsteps:		.byte 2		;maximum steps per second
divratio:		.byte 2		;16 bit divison ratio (fraction) spindle count /encoder count l:f
rotation:		.byte 1		;rotation 1= forwards 0=reversed
maxset:			.byte 1		;maximum value for SET button
maxleft:		.byte 1		;maximum value for LEFT button
maxdown:		.byte 1		;maximum value for DOWN button
maxup:			.byte 1		;maximum value for UP button
maxright:		.byte 1		;maximum value for RIGHT button











.CSEG

; Reset- and Interrupt-vectors

 jmp RESET ; Reset
 jmp encoderrd; INT0 ; IRQ0
 jmp encoderrd;INT01 ; IRQ1
 jmp null;PCINT0 ; PCINT0
 jmp null;PCINT1 ; PCINT1
 jmp null;PCINT2 ; PCINT2
 jmp null;WDT ; Watchdog Timeout
 jmp null;TIM2_COMPA ; Timer2 CompareA
 jmp null;TIM2_COMPB ; Timer2 CompareB
 jmp TIM2_OVF ; Timer2 Overflow
 jmp null;TIM1_CAPT ; Timer1 Capture
 jmp null;TIM1_COMPA ; Timer1 CompareA
 jmp null;TIM1_COMPB ; Timer1 CompareB
 jmp TIM1_OVF ; Timer1 Overflow
 jmp null;TIM0_COMPA ; Timer0 CompareA
 jmp null;TIM0_COMPB ; Timer0 CompareB
 jmp TIM0_OVF ; Timer0 Overflow
 jmp null;SPI_STC ; SPI Transfer Complete
 jmp null;USART_RXC ; USART RX Complete
 jmp null;USART_UDRE ; USART UDR Empty
 jmp null;USART_TXC ; USART TX Complete
 jmp null;ADC ; ADC Conversion Complete
 jmp null;EE_RDY ; EEPROM Ready
 jmp null;ANA_COMP ; Analog Comparator
 jmp null ; 2-wire Serial
 jmp null;SPM_RDY ; SPM Ready




  TIM0_OVF:							;every 16ms
	in		rSrg,SREG ; Save SREG
	sbr		flags,0x08				;set t0 ovf flag (key delays)

	cbr		flags,0x10				;clear moving flag
	sbrc	flags,6					;stepped flag
		sbr		flags,0x10			;set moving flag if steps in last 16ms
	cbr		flags,0x40				;clear stepped flag

	cbr		flags2,0x01				;clear overflow flag
	lds		rimp2,rpmcount
	lds		rimp,rpmcount+1			;divide count by 4
	lsr		rimp2
	ror		rimp
	lsr		rimp2
	ror		rimp
	sts		rpmpre,rimp				;scaled value (8 bit)
	tst		rimp2
	breq	t0a
		sbr		flags2,0x01			;if high byte >0 set overflow flag
t0a:
	clr		rimp
	sts		rpmcount,rimp
	sts		rpmcount+1,rimp			;zero
	sbr		flags2,0x02				;rpm value ready
		

T0end:
	out		SREG,rSrg ; restore SREG to original content
	reti

TIM1_OVF:
	in		rSrg,SREG ; Save SREG

	clr		rimp
	sts		TCCR1B,rimp			;no clock (stopped) ,ldi	0b00000010 =/8 (.05us tick)
	ldi		rimp,0xff			;set clock to overflow-128us (step pulse width)
	sts		TCNT1H,rimp			;WRITE high byte first READ low byte first
	ldi		rimp,0x0f
	sts		TCNT1L,rimp

	cbi		PORTC,1				;step pin off
		
T1end:
	out		SREG,rSrg			 ; restore SREG to original content
	reti

TIM2_OVF:
	in		rSrg,SREG			; Save SREG

	tst		step				;check sign and if zero
	brne	T2_01 
		sbr		flags,0x20		;timeout with no steps, set step immediately flag
			rjmp	T2end

T2_01:	cbr		flags,0x20		;clear step immediately flag (next step can't occur before timeout period)
		tst		step
		brmi	T2_50			;step is negative
											;step positive:
			sbrc	flags2,3		;reverse rotation flag
				sbi		PORTC,2			;DIR pin, direction reversed
			sbrs	flags2,3		;reverse rotation flag
				cbi		PORTC,2			;DIR pin, direction normal

			rjmp t2_st



T2_50:		
										;step negative:
			sbrs	flags2,3			;reverse rotation flag
				sbi		PORTC,2			;DIR pin, direction reversed
			sbrc	flags2,3			;reverse rotation flag
				cbi		PORTC,2			;DIR pin, direction normal
			
			;rjmp	T2_stmi
	


T2_stmi:							;step only, no inc/dec speed (minus, reverse direction)
		sbi		PORTC,1				;set step pin
		ldi		rimp,0b00000010		;T1 clock on, T1 ISR will turn off step pulse
		sts		TCCR1B,rimp			;120us
		inc		step				;INCREASE step counter ( reverse direction)
		breq	T2end
			sbr		flags,0x80		;set increase speed flag if still steps in buffer
			rjmp	T2end


T2_st:								;step only, no inc speed
		sbi		PORTC,1				;set step pin
		ldi		rimp,0b00000010		;T1 clock on, T1 ISR will turn off step pulse
		sts		TCCR1B,rimp			;120us
		dec		step				;decrease step counter
		breq	T2end
			sbr		flags,0x80		;set increase speed flag if still steps in buffer


T2end:
	sts		TCNT2,T2reload			;reload speed period
	out		SREG,rSrg				; restore SREG to original content
	reti


encoderrd:				;encoder, pin change on int0 or int1. int0 XOR previous int1
	in		rSrg,SREG	; Save SREG

	lds		rimp,rpmcount+1
	inc		rimp
	sts		rpmcount+1,rimp
	brne	enc0
		lds		rimp,rpmcount
		inc		rimp
		sts		rpmcount,rimp
enc0:
	in		rimp,pind		;int0 bit 2, int1 bit3
	sts		ints,rimp
	andi	rimp,0x04		;isolate int0
	lds		rimp2,oldint1
	eor		rimp,rimp2
	breq	enc1
		lds		rimp,divratio+1			;add division ratio
		lds		rimp2,divratio
		add		encoderf,rimp
		adc		encoderl,rimp2			;if encoderl increases, step forward
		eor		encoderlold,encoderl
		breq	encend
			inc		mdtally					;increase tally, if matches divison, clear and add step
			cp		maindiv,mdtally
			brne	encend
				clr		mdtally			
				sbr		flags,0x40			;stepped
				sbr		flags,0x10			;set moving flag 
				inc		step
				ldi		rimp,0xff			;min t2 reload 64us
				sbrc	flags,5				;step immediately
					sts		TCNT2,rimp			;reload speed period (min)

				brvc	encend				;if overflow, acceleration is too low to keep up
					sbr		flags,0x04		;lost lock flag
					rjmp	encend

enc1:
		lds		rimp,divratio+1			;subtract division ratio
		lds		rimp2,divratio
		sub		encoderf,rimp
		sbc		encoderl,rimp2			;if encoderl decreases, step backward
		eor		encoderlold,encoderl
		breq	encend
			dec		mdtally					;decrease tally, if 0xff, reload to division-1, dec step
			ldi		rimp,0xff
			cp		mdtally,rimp
			brne	encend
				mov		mdtally,maindiv
				dec		mdtally
				sbr		flags,0x40			;stepped
				sbr		flags,0x10			;set moving flag 
				dec		step
				ldi		rimp,0xff			;min t2 reload 64us
				sbrc	flags,5				;step immediately
					sts		TCNT2,rimp			;reload speed period (min)
				brvc	encend				;if overflow, acceleration is too low to keep up
					sbr		flags,0x04
					rjmp	encend

encend:
	lds		rimp,ints		;current value
	ror		rimp			;shift int1 bit into int0 position for comparison
	andi	rimp,0x04		;isolate bit
	sts		oldint1,rimp
	mov		encoderlold,encoderl



	out		SREG,rSrg ; restore SREG to original content
	reti



div16u:	
	clr	r0		;clear remainder Low byte
	sub	r1,r1			;clear remainder High byte and carry
	ldi	counter,17		;init loop counter
d16u_1:	rol	m16l			;shift left dividend
	rol	m16h
	dec	counter			;decrement counter
	brne	d16u_2			;if done
	ret				;    return
d16u_2:	rol	r0		;shift dividend into remainder
	rol	r1
	sub	r0,rmp		;remainder = remainder - divisor
	sbc	r1,rmp2		;
	brcc	d16u_3			;if result negative
	add	r0,rmp		;    restore remainder
	adc	r1,rmp2
	clc				;    clear carry to be shifted into result
	rjmp	d16u_1			;else
d16u_3:	sec				;    set carry to be shifted into result
	rjmp	d16u_1




;24 by 16 divide (H:M:L/M:L)     (modified avr200 "div16u" - 16/16 Bit Unsigned Division)
;dividend/result: m16H:m16l:m16f (r3,4,5)
;divisor	rmp2:rmp
;remainder R2:R1:R0
;counter for loop counter





div2416u:	clr		r0				;clear remainder Low byte
			clr		r1				;clear remainder mid byte
			sub		r2,r2			;clear remainder High byte and carry
			ldi		counter,25		;init loop counter
d2416u_1:	rol		m16f			;shift left dividend
			rol		m16l
			rol		m16H
			dec		counter			;decrement counter
			brne	d2416u_2			;if done
				ret				;    return
d2416u_2:	rol		r0				;shift dividend into remainder
			rol		r1
			rol		r2
			sub		r0,rmp				;remainder = remainder - divisor. MSByte not subtracted since divisor always 0 (16 bit)
			sbc		r1,rmp2				; high byte
			brcc	d2416u_3			;if result negative
				add	r0,rmp				;    restore remainder
				adc	r1,rmp2
				clc						;    clear carry to be shifted into result
				rjmp	d2416u_1			;else
d2416u_3:	sec							;    set carry to be shifted into result
			rjmp	d2416u_1

calcacc:								;calculate acc delta value from accelartion in steps sec^2, =(acceleration /210) +1 (value 1-48)
			lds		m16h,acceleration
			lds		m16l,acceleration+1
			ldi		rmp2,0x00				;210	
			ldi		rmp,0xd2		
			rcall	div16u
			inc		m16l
			sts		acc,m16l
			ret

calcrpm:								;calculate rpm multiplier, 14648/encoder pulses (x4 enc)
			ldi		rmp,0x39
			mov		m16h,rmp
			ldi		rmp,0x38
			mov		m16l,rmp
			lds		rmp2,stepsrencoder				;210	
			lds		rmp,stepsrencoder+1		
			rcall	div16u
			sts		rpmmult,m16l
			ret


copy2ram:									;copy eeprom settings to RAM 
			clr		rmp
			mov		r0,rmp					;start of eeseg in r1:r0		
			mov		r1,rmp	
			ldi		YH,high(checksum)		;put RAM table start address into Y 
			ldi		YL,low(checksum)
			ldi		counter,0x16			;number of bytes to transfer
c2rloop:			call	eeprom_read
			st		Y+,rmp
			inc		r0
			brne	c2r1
			inc		r1
c2r1:		dec		counter
			brne	c2rloop
			ret

save2eeprom:								;save settings in ram to eeprom
			clr		rmp
			mov		r0,rmp					;start of eeseg in r1:r0		
			mov		r1,rmp	
			ldi		YH,high(checksum)		;put RAM table start address into Y 
			ldi		YL,low(checksum)
			ldi		counter,0x16			;number of bytes to transfer
s2eloop:			ld		rmp,Y+
			call	eeprom_write
			inc		r0
			brne	s2e1
			inc		r1
s2e1:		dec		counter
			brne	s2eloop
			ret

EEPROM_read:					;address in r1:r0, return with data in rmp

			sbic EECR,EEPE		; Wait for completion of previous write
			rjmp EEPROM_read
			out EEARH, r1
			out EEARL, r0

			sbi EECR,EERE			; Start eeprom read by writing EERE

			in rmp,EEDR				; Read data from Data Register
		ret

EEPROM_write:						;address in r1:r0, write data in rmp

			sbic EECR,EEPE			; Wait for completion of previous write
			rjmp EEPROM_write
			out EEARH, r1
			out EEARL, r0
			out EEDR,rmp			; Write data (r16) to Data Register
			cli
			sbi EECR,EEMPE			; Write logical one to EEMPE
			sbi EECR,EEPE			; Start eeprom write by setting EEPE
			sei
		ret



delay1us:
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	ret



delay50us:		;50us delay (uses rmp)
	push	rmp
	ldi		rmp,0x32
d50ua:
	dec		rmp
	rcall	delay1us
	brne	d50ua
	pop		rmp
	ret

delay16ms:		;16ms delay using tmr0
	push	rmp
	push	rmp2
	wdr
	in		rmp,TCNT0
	dec		rmp				;= +255x64us 
d16a:
	in		rmp2,TCNT0		;wait for match (16ms)
	cp		rmp,rmp2
	brne	d16a

	pop		rmp2
	pop		rmp
	ret

delay2ms:		;2ms delay using tmr0
	push	rmp
	push	rmp2
	wdr
	in		rmp,TCNT0
	ldi		rmp2,0x20
	add		rmp,rmp2		;= +32x64us 
d2a:
	in		rmp2,TCNT0		;wait for match (2ms)
	cp		rmp,rmp2
	brne	d2a

	pop		rmp2
	pop		rmp
	ret


delay200ms:
	push	counter
	ldi		counter,0x0c
d200a:
	rcall	delay16ms
	dec		counter
	brne	d200a
	pop		counter
	ret

delay5ms:		;2ms delay using tmr0
	push	rmp
	push	rmp2
	wdr
	in		rmp,TCNT0
	ldi		rmp2,0x4e
	add		rmp,rmp2		;= +78x64us 
d5a:
	in		rmp2,TCNT0		;wait for match (5ms)
	cp		rmp,rmp2
	brne	d5a

	pop		rmp2
	pop		rmp
	ret

delay3s:								;with clear screen and delay after
				ldi		rmp2,0x0f
delay3s2:		rcall	delay200ms
				dec		rmp2
				brne	delay3s2
del3done:		
				ldi		rmp,0x01		;clear display (1.64ms)
				rcall	lcd_com4
				rcall	delay200ms
				ret

delay3sk:								;bail if key pressed, return with key in buffer
				ldi		rmp2,0x0a
delay3sk2:		rcall	delay200ms
				rcall	checkeys
				lds		rmp,keybuf
				tst		rmp
				brne	del3kdone
				dec		rmp2
				brne	delay3sk2
del3kdone:		
				ldi		rmp,0x01		;clear display (1.64ms)
				rcall	lcd_com4
				rcall	delay200ms
				ret



bcd2bin:				;16bit no in BCDH:BCDL to rmp2:rmp
	clr		r0
	clr		r1

	ldi		rmp2,0x03			;1000
	ldi		rmp,0xe8
	mov		counter,bcdh
	swap	counter
	andi	counter,0x0f
bcd2bina:
	breq	bcd2binb
		add		r0,rmp			;add 1000s
		adc		r1,rmp2
		dec		counter
		rjmp	bcd2bina
bcd2binb:
	ldi		rmp2,0x00			;100
	ldi		rmp,0x64
	mov		counter,bcdh
	andi	counter,0x0f
bcd2binc:
	breq	bcd2bind
		add		r0,rmp			;add 100s
		adc		r1,rmp2
		dec		counter
		rjmp	bcd2binc
bcd2bind:
	ldi		rmp2,0x00			;10
	ldi		rmp,0x0a
	mov		counter,bcdl
	swap	counter
	andi	counter,0x0f
bcd2bine:
	breq	bcd2binf
		add		r0,rmp			;add 10s
		adc		r1,rmp2
		dec		counter
		rjmp	bcd2bine
bcd2binf:
	mov		rmp,bcdl
	andi	rmp,0x0f
	add		r0,rmp			;add 1s
	adc		r1,rmp2

	mov		rmp,r0
	mov		rmp2,r1
	ret



bin2bcd:			;16bit binary in rmp2:rmp to 4 digit packed BCD in BCDH:BCDL 

	clr		BCDH	;clear result
	clr		BCDL
	clr		r1

	ldi		counter,0xe8	;1000		(counter used as rmp&rmp2 in use)
	mov		r0,counter
	ldi		counter,0x03	;		(counter used as rmp&rmp2 in use)
	mov		r1,counter

bin00:

	sub		rmp,r0			;subtract 100 (16 bit)
	sbc		rmp2,r1	
	brcs	bin0
	inc		BCDH			;increase (hundreds) BCDH digit
	rjmp	bin00			;continue until result<100
bin0:
	add		rmp,r0			; underflow, add last 1000 back
	adc		rmp2,r1
	swap	BCDH			;swap result into high nibble (thousands)


	ldi		counter,0x64	;100		(counter used as rmp&rmp2 in use)
	mov		r0,counter
	clr		r1

bin1:

	sub		rmp,r0			;subtract 100 (16 bit)
	sbc		rmp2,r1	
	brcs	bin2
	inc		BCDH			;increase hundreds BCD digit
	rjmp	bin1			;continue until result<100
bin2:
	add		rmp,r0			; underflow, add last 100 back
	adc		rmp2,r1

bin3:						;rmp2:rmp is now 0-99dec so rmp2=0 and can be ignored
	ldi		counter,0x0a	;10
	sub		rmp,counter		;count tens by subtraction
	brcs	bin4	
	ldi		rmp2,0x10	
	inc		BCDL			;increase tens BCD digit (INC LO NIBBLE THEN SWAP)
	rjmp	bin3
bin4:
	add		rmp,counter		; underflow, add last 10 back
	swap	BCDL			;PUT TENS DIGIT IN HIGH NIBBLE
	or		BCDL,rmp		;whats left is units, put in BCDL lo nibble
	ret







lcd_com:		;write command to LCD (from rmp) uses rmp and rmp2


			cbi		PORTB,RS			;clear RS	(command)

			ldi		rmp2,0x0c		;pullups for encoder
			andi	rmp,0xf0
			or		rmp2,rmp
			out		PORTD,rmp2		;put data on bus
			nop						;min setup time (Tas) 140ns
			nop
			sbi		PORTB,E			;Set E
			nop
			nop
			nop						;Min PWh 450ns
			nop
			nop
			sbi		PINB,5			 
			nop
			cbi		PORTB,E			;clear E

			ret

lcd_com4:		;write command to LCD (from rmp) uses rmp and rmp2 ***4 BIT BUS


			cbi		PORTB,RS			;clear RS	(command)
			mov		data,rmp
			ldi		rmp2,0x0c		;pullups for encoder
			andi	rmp,0xf0
			or		rmp2,rmp		;high nibble
			out		PORTD,rmp2		;put data on bus
			nop						;min setup time (Tas) 140ns
			nop
			sbi		PORTB,E			;Set E
			nop
			nop
			nop						;Min PWh 450ns
			nop
			nop
			sbi		PINB,5 
			nop
			cbi		PORTB,E			;clear E

			mov		rmp,data
			swap	rmp
			ldi		rmp2,0x0c		;pullups for encoder
			andi	rmp,0xf0
			or		rmp2,rmp		;low nibble
			out		PORTD,rmp2		;put data on bus
			nop						;min setup time (Tas) 140ns
			nop
			sbi		PORTB,E			;Set E
			nop
			nop
			nop						;Min PWh 450ns
			nop
			nop
			nop
			nop
			cbi		PORTB,E			;clear E

			ret

lcd_data:								;write	data to LCD (from rmp)

			sbi		PORTB,RS			;set RS	(data)
	
			ldi		rmp2,0x0c		;pullups for encoder
			andi	rmp,0xf0
			or		rmp2,rmp
			out		PORTD,rmp2		;put data on bus
			nop						;min setup time (Tas) 140ns
			nop
			sbi		PORTB,E			;Set E
			nop
			nop
			nop						;Min PWh 450ns
			nop
			nop
			nop
			nop
			cbi		PORTB,E			;clear E

			ret

lcd_data4:		;write data to LCD (from rmp) uses rmp and rmp2 ***4 BIT BUS


			sbi		PORTB,RS			;set RS	(data)
			mov		data,rmp
			ldi		rmp2,0x0c		;pullups for encoder
			andi	rmp,0xf0
			or		rmp2,rmp		;high nibble
			out		PORTD,rmp2		;put data on bus
			nop						;min setup time (Tas) 140ns
			nop
			sbi		PORTB,E			;Set E
			nop
			nop
			nop						;Min PWh 450ns
			nop
			nop
			nop
			nop
			cbi		PORTB,E			;clear E

			mov		rmp,data
			swap	rmp
			ldi		rmp2,0x0c		;pullups for encoder
			andi	rmp,0xf0
			or		rmp2,rmp		;low nibble
			out		PORTD,rmp2		;put data on bus
			nop						;min setup time (Tas) 140ns
			nop
			sbi		PORTB,E			;Set E
			nop
			nop
			nop						;Min PWh 450ns
			nop
			nop
			nop
			nop
			cbi		PORTB,E			;clear E

			ret
dispnum:								;display bcd number on line 2
			
			cbr		flags,0x01			;clear key press flag

			ldi		rmp,0x0c		;cursor off
			rcall	lcd_com4
			rcall	delay50us

			ldi		rmp,0xc0		;set cursor position, start
			rcall	lcd_com4
			

			

dispnumfree:								;display bcd number (positon set before)
			
			
			rcall	delay50us
			mov 	rmp,bcdh			;thousands
			swap	rmp
			andi	rmp,0x0f
			ldi		rmp2,0x30
			add		rmp,rmp2
			rcall	lcd_data4
			rcall	delay50us

			mov		rmp,bcdh			;hundreds
			andi	rmp,0x0f
			ldi		rmp2,0x30
			add		rmp,rmp2
			rcall	lcd_data4
			rcall	delay50us

			mov		rmp,bcdl			;tens
			swap	rmp
			andi	rmp,0x0f
			ldi		rmp2,0x30
			add		rmp,rmp2
			rcall	lcd_data4
			rcall	delay50us

			mov		rmp,bcdl			;units			
			andi	rmp,0x0f
			ldi		rmp2,0x30
			add		rmp,rmp2
			rcall	lcd_data4
			rcall	delay50us
			ret

valueadj:

			clr		rmp					;start at 0
			sts		pos,rmp

			
su1:
			wdr

			sbrc	flags,0				;skip unless key pressed
			rcall	dispnum

			

			lds		rmp2,pos
			ldi		rmp,0xc0		;set cursor position
			add		rmp,rmp2
			rcall	lcd_com4
			rcall	delay50us

			ldi		rmp,0x0d		;cursor on
			rcall	lcd_com4
			rcall	delay50us
	
			rcall	checkeys
			lds		rmp,keybuf
				cpi		rmp,0x01		;right
				brne	su1a
					lds		rmp,pos		;move cursor right
					inc		rmp
					andi	rmp,0x03	
					sts		pos,rmp
					clr		rmp
					sts		keybuf,rmp			;remove from buffer
su1a:
			lds		rmp,keybuf
				cpi		rmp,0x04				;left
				brne	su1b
					lds		rmp,pos				;move cursor left
					dec		rmp
					andi	rmp,0x03
					sts		pos,rmp	
					clr		rmp
					sts		keybuf,rmp			;remove from buffer
su1b:
			lds		rmp,keybuf
				cpi		rmp,0x02				;up
				brne	su1c						
					clr		rmp
					sts		keybuf,rmp			;remove from buffer
					sbr		flags,0x02			;set updated value flag
					


					lds		rmp,pos				;check which digit to increase
					cpi		rmp,0x00
					brne	sub1b1
						mov		rmp,bcdh		;1st digit
						ldi		rmp2,0x10
						add		rmp,rmp2
						mov		rmp2,rmp
						andi	rmp2,0xf0
						push	rmp
						lds		rmp,msdbcd		;max digit value (+1)
						cp		rmp2,rmp		;limit to max 2 for fist digit only
						pop		rmp
						breq	su1c
							mov		bcdh,rmp	;store updated value
							rjmp	su1c
sub1b1:
					lds		rmp,pos				;check which digit to increase
					cpi		rmp,0x01
					brne	sub1b2
						mov		rmp,bcdh		;2st digit
						inc		rmp
						mov		rmp2,rmp
						andi	rmp2,0x0f
						cpi		rmp2,0x0a		;limit to max 9
						breq	su1c
							mov		bcdh,rmp	;store updated value
							rjmp	su1c
sub1b2:				
					lds		rmp,pos				;check which digit to increase
					cpi		rmp,0x02
					brne	sub1b3
						mov		rmp,bcdl		;3rd digit
						ldi		rmp2,0x10
						add		rmp,rmp2
						mov		rmp2,rmp
						andi	rmp2,0xf0
						cpi		rmp2,0xa0		;limit to max 9
						breq	su1c
							mov		bcdl,rmp	;store updated value
							rjmp	su1c
sub1b3:
					lds		rmp,pos				;check which digit to increase
					cpi		rmp,0x03
					brne	su1c
						mov		rmp,bcdl		;4th digit
						inc		rmp
						mov		rmp2,rmp
						andi	rmp2,0x0f
						cpi		rmp2,0x0a		;limit to max 9
						breq	su1c
							mov		bcdl,rmp	;store updated value
							
su1c:		
				lds		rmp,keybuf
				cpi		rmp,0x03				;down
				brne	su3						
					clr		rmp
					sts		keybuf,rmp			;remove from buffer
					sbr		flags,0x02			;set updated value flag
					
	


					lds		rmp,pos				;check which digit to increase
					cpi		rmp,0x00
					brne	sub2b1
						mov		rmp,bcdh		;1st digit
						ldi		rmp2,0x10
						sub		rmp,rmp2
						brcs	su2c			;limit min to 0
							mov		bcdh,rmp	;store updated value
							rjmp	su2c
sub2b1:
					lds		rmp,pos				;check which digit to increase
					cpi		rmp,0x01
					brne	sub2b2
						mov		rmp,bcdh		;2st digit
						dec		rmp
						mov		rmp2,rmp
						andi	rmp2,0x0f
						cpi		rmp2,0x0f		;limit min to 0
						breq	su2c
							mov		bcdh,rmp	;store updated value
							rjmp	su2c
sub2b2:				
					lds		rmp,pos				;check which digit to increase
					cpi		rmp,0x02
					brne	sub2b3
						mov		rmp,bcdl		;3rd digit
						ldi		rmp2,0x10
						sub		rmp,rmp2
						brcs	su2c			;limit min to 0
							mov		bcdl,rmp	;store updated value
							rjmp	su2c
sub2b3:
					lds		rmp,pos				;check which digit to increase
					cpi		rmp,0x03
					brne	su2c
						mov		rmp,bcdl		;4th digit
						dec		rmp
						mov		rmp2,rmp
						andi	rmp2,0x0f
						cpi		rmp2,0x0f		;limit min to 0
						breq	su2c
							mov		bcdl,rmp	;store updated value
su2c:
su3:			lds		rmp,keybuf
				cpi		rmp,0x05				;setup (end)
				brne	su4					
					clr		rmp
					sts		keybuf,rmp			;remove from buffer
					ldi		rmp,0x0c			;cursor on
					rcall	lcd_com4
					rcall	delay50us
					ret			
su4:				rjmp	su1

valueadj100:									;coarse adjust, steps of 100 
		
sv1:
			wdr

			sbrc	flags,0						;skip unless key pressed
			rcall	dispnum

						rcall	checkeys

		
sv1b:
			lds		rmp,keybuf
				cpi		rmp,0x02				;up
				brne	sv2						
					clr		rmp
					sts		keybuf,rmp			;remove from buffer
					sbr		flags,0x02			;set updated value flag
					
					inc		bcdh
					mov		rmp,bcdh
					andi	rmp,0x0f				;check digit overflow
					cpi		rmp,0x0a
					brne	sv1
						mov		rmp,bcdh			;digit overflow, increase thousands unless =9
						ldi		rmp2,0x10
						add		rmp,rmp2
						andi	rmp,0xf0
						cpi		rmp,0xa0
						brne	sv1c	
							dec		bcdh			;reached max, restore hundreds digit
							rjmp	sv1
sv1c:					mov		bcdh,rmp
						rjmp		sv1
						

							
sv2:		
				lds		rmp,keybuf
				cpi		rmp,0x03				;down
				brne	sv3						
					clr		rmp
					sts		keybuf,rmp			;remove from buffer
					sbr		flags,0x02			;set updated value flag
					
					dec		bcdh
					brne	sv2a				;if reaches 00 set to 01
						inc		bcdh
						rjmp	sv1
sv2a:				mov		rmp,bcdh
					andi	rmp,0x0f				;check digit underflow
					cpi		rmp,0x0f
					brne	sv1
						mov		rmp,bcdh			;digit underrflow,  reload hundreds with 9
						andi	rmp,0xf0
						ori		rmp,0x09
						mov		bcdh,rmp
						rjmp	sv1


sv3:			lds		rmp,keybuf
				cpi		rmp,0x05				;setup (end)
				brne	sv1						
					clr		rmp
					sts		keybuf,rmp			;remove from buffer
					ret			


configbuts:
			ldi		ZH,high(mssgcb1<<1)
			ldi		ZL,low(mssgcb1<<1)
			ldi		rmp,0x80					;set pos line 1 start
			rcall	lcd_com4
			rcall	delay50us
			rcall	mssgout

			ldi		ZH,high(mssgcb2<<1)
			ldi		ZL,low(mssgcb2<<1)
			ldi		rmp,0xc0					;set pos line 2 start
			rcall	lcd_com4
			rcall	delay50us
			rcall	mssgout		

cfbdel:				lds		rmp,ADCSRA
				ori		rmp,0x40			;set start conversion bit
				sts		ADCSRA,rmp

			rcall	delay3s

			rcall	waitnokey

			ldi		ZH,high(mssgpress<<1)
			ldi		ZL,low(mssgpress<<1)
			ldi		rmp,0x80					;set pos line 1 start
			rcall	lcd_com4
			rcall	delay50us
			rcall	mssgout

			ldi		ZH,high(mssgset<<1)
			ldi		ZL,low(mssgset<<1)
			ldi		rmp,0xc0					;set pos line 2 start
			rcall	lcd_com4
			rcall	delay50us
			rcall	mssgout	

readset:	rcall	reada2d
			cpi		rmp,0xf0
			brcc	readset					;wait until key pressed
			subi	rmp,0xf8				;add margin of 8
			sts		maxset,rmp

			rcall	waitnokey


			ldi		ZH,high(mssgl<<1)
			ldi		ZL,low(mssgl<<1)
			ldi		rmp,0xc0					;set pos line 2 start
			rcall	lcd_com4
			rcall	delay50us
			rcall	mssgout	

readleft:	rcall	reada2d
			cpi		rmp,0xf0
			brcc	readleft					;wait until key pressed
			subi	rmp,0xf8				;add margin of 8
			sts		maxleft,rmp

			rcall	waitnokey


			ldi		ZH,high(mssgd<<1)
			ldi		ZL,low(mssgd<<1)
			ldi		rmp,0xc0					;set pos line 2 start
			rcall	lcd_com4
			rcall	delay50us
			rcall	mssgout	

readdown:	rcall	reada2d
			cpi		rmp,0xf0
			brcc	readdown					;wait until key pressed
			subi	rmp,0xf8				;add margin of 8
			sts		maxdown,rmp

			rcall	waitnokey

			ldi		ZH,high(mssgu<<1)
			ldi		ZL,low(mssgu<<1)
			ldi		rmp,0xc0					;set pos line 2 start
			rcall	lcd_com4
			rcall	delay50us
			rcall	mssgout	

readup:		rcall	reada2d
			cpi		rmp,0xf0
			brcc	readup					;wait until key pressed
			subi	rmp,0xf8				;add margin of 8
			sts		maxup,rmp

			rcall	waitnokey

			ldi		ZH,high(mssgr<<1)
			ldi		ZL,low(mssgr<<1)
			ldi		rmp,0xc0					;set pos line 2 start
			rcall	lcd_com4
			rcall	delay50us
			rcall	mssgout	

readright:	rcall	reada2d
			cpi		rmp,0xf0
			brcc	readright				;wait until key pressed
			subi	rmp,0xf8				;add margin of 8
			sts		maxright,rmp

			ldi		rmp,0x01					;clear display (1.64ms)
			rcall	lcd_com4
			rcall	delay200ms
			rcall	save2eeprom				;save updated values
			ret

reada2d:									;read with wait for conversion				
				lds		rmp,ADCSRA
				ori		rmp,0x40			;set start conversion bit
				sts		ADCSRA,rmp
				rcall	delay2ms
				lds		rmp,ADCH			;read A2D value 
				ret
					
waitnokey:		rcall	delay200ms
				lds		rmp,ADCSRA			;wait until all keys released
				ori		rmp,0x40			;set start conversion bit
				sts		ADCSRA,rmp
				rcall	delay2ms
				lds		rmp,ADCH			;read A2D value 
				cpi		rmp,0xf0
				brcs	waitnokey
				ret

setup:		ldi		rmp,0x01					;clear display (1.64ms)
			rcall	lcd_com4
			rcall	delay200ms
			ldi		ZH,high(stepenc<<1)
			ldi		ZL,low(stepenc<<1)
			ldi		rmp,0x80					;set pos line 1 start
			rcall	lcd_com4
			rcall	delay50us
			rcall	mssgout

			ldi		rmp,0xc0					;set pos line 2 start
			rcall	lcd_com4
			rcall	delay50us

			ldi		rmp,0x30					;2+1 in MSD (max value)
			sts		msdbcd,rmp

			lds		rmp2,stepsrencoder			;steps
			lds		rmp,stepsrencoder+1
			lsr		rmp2						;/4 for lines/CPR
			ror		rmp
			lsr		rmp2
			ror		rmp
			rcall	bin2bcd

			sbr		flags,0x01				;set key press flag so value displayed in subr
			cbr		flags,0x02				;clear update settings flag
			rcall	valueadj

			
			rcall	bcd2bin					;store updated value
			lsl		rmp						;CPR*4 for steps
			rol		rmp2
			lsl		rmp 
			rol		rmp2
			sts		stepsrencoder,rmp2
			sts		stepsrencoder+1,rmp
stpspin:
			ldi		rmp,0x01				;clear display (1.64ms)
			rcall	lcd_com4
			rcall	delay200ms
			ldi		ZH,high(stepspin<<1)
			ldi		ZL,low(stepspin<<1)
			ldi		rmp,0x80				;set pos line 1 start
			rcall	lcd_com4
			rcall	delay50us
			rcall	mssgout

			ldi		rmp,0xc0				;set pos line 2 start
			rcall	lcd_com4		
			rcall	delay50us

			lds		rmp2,stepsrspindle
			lds		rmp,stepsrspindle+1
			rcall	bin2bcd

			sbr		flags,0x01				;set key press flag so value displayed in subr
			rcall	valueadj

			rcall	bcd2bin					;store updated value
			sts		stepsrspindle,rmp2
			sts		stepsrspindle+1,rmp

			lds		r1,stepsrencoder		;check encoder>=spindle
			lds		r0,stepsrencoder+1
			sub		r0,rmp
			sbc		r1,rmp2
			brcc	accel
				ldi		rmp,0x01			;clear display (1.64ms)
				rcall	lcd_com4
				rcall	delay200ms

				ldi		ZH,high(emssg1<<1)
				ldi		ZL,low(emssg1<<1)
				ldi		rmp,0x80			;set pos line 1 start
				rcall	lcd_com4
				rcall	delay50us
				rcall	mssgout

				ldi		ZH,high(emssg2<<1)
				ldi		ZL,low(emssg2<<1)
				ldi		rmp,0xc0			;set pos line 1 start
				rcall	lcd_com4
				rcall	delay50us
				rcall	mssgout

				rcall	delay3s



				rjmp	setup



accel:		
		
			ldi		rmp,0x01			;clear display (1.64ms)
			rcall	lcd_com4
			rcall	delay200ms
			ldi		ZH,high(accmssg<<1)
			ldi		ZL,low(accmssg<<1)
			ldi		rmp,0x80			;set pos line 1 start
			rcall	lcd_com4
			rcall	delay50us
			rcall	mssgout

			ldi		rmp,0xc0			;set pos line 2 start
			rcall	lcd_com4
			rcall	delay50us
			ldi		ZH,high(accmssg2<<1)
			ldi		ZL,low(accmssg2<<1)
			rcall	delay50us
			rcall	mssgout
			ldi		rmp,0xc0			;set pos line 2 start
			rcall	lcd_com4
			rcall	delay50us


			lds		rmp2,acceleration
			lds		rmp,acceleration+1
			rcall	bin2bcd

			sbr		flags,0x01				;set key press flag so value displayed in subr
			rcall	valueadj100	
			
			rcall	bcd2bin					;store updated value
			sts		acceleration,rmp2
			sts		acceleration+1,rmp	

speed0:		ldi		rmp,0x01				;clear display (1.64ms)
			rcall	lcd_com4
			rcall	delay200ms
			ldi		ZH,high(spmssg<<1)
			ldi		ZL,low(spmssg<<1)
			ldi		rmp,0x80				;set pos line 1 start
			rcall	lcd_com4
			rcall	delay50us
			rcall	mssgout

			ldi		rmp,0xc0				;set pos line 2 start
			rcall	lcd_com4
			rcall	delay50us
			ldi		ZH,high(spmssg2<<1)
			ldi		ZL,low(spmssg2<<1)
			rcall	delay50us
			rcall	mssgout
			ldi		rmp,0xc0				;set pos line 2 start
			rcall	lcd_com4
			rcall	delay50us

			ldi		rmp,0x60				;5+1 in MSD (max value)
			sts		msdbcd,rmp


			lds		rmp2,maxsteps
			lds		rmp,maxsteps+1
			rcall	bin2bcd

			sbr		flags,0x01				;set key press flag so value displayed in subr
			rcall	valueadj	
			
			rcall	bcd2bin					;store updated value

			ldi		counter,0x63			;min value 100
			mov		r0,counter
			clr		r1
			sub		r0,rmp
			sbc		r1,rmp2
			brcs	speed1					;if carry is >=100
				ldi		rmp,0x01			;clear display (1.64ms)
				rcall	lcd_com4
				rcall	delay200ms
				ldi		ZH,high(emssg15<<1)
				ldi		ZL,low(emssg15<<1)
				ldi		rmp,0x80			;set pos line 1 start
				rcall	lcd_com4
				rcall	delay50us
				rcall	mssgout

				ldi		rmp,0xc0			;set pos line 2 start
				rcall	lcd_com4
				rcall	delay50us
				ldi		ZH,high(emssg16<<1)
				ldi		ZL,low(emssg16<<1)
				rcall	delay50us
				rcall	mssgout
				ldi		rmp,0xc0			;set pos line 2 start
				rcall	lcd_com4
				rcall	delay3s
				clr		rmp
				sts		maxsteps,rmp
				ldi		rmp,0x64
				sts		maxsteps+1,rmp
				rjmp	speed0

speed1:			ldi		counter,0x13			;max value 5000
				mov		r1,counter
				ldi		counter,0x88			
				mov		r0,counter
				sub		r0,rmp
				sbc		r1,rmp2
				brcc	speeddone			;not set, <5000
					ldi		rmp,0x01		;clear display (1.64ms)
					rcall	lcd_com4
					rcall	delay200ms
					ldi		ZH,high(emssg15<<1)
					ldi		ZL,low(emssg15<<1)
					ldi		rmp,0x80		;set pos line 1 start
					rcall	lcd_com4
					rcall	delay50us
					rcall	mssgout

					ldi		rmp,0xc0		;set pos line 2 start
					rcall	lcd_com4
					rcall	delay50us
					ldi		ZH,high(emssg17<<1)
					ldi		ZL,low(emssg17<<1)
					rcall	delay50us
					rcall	mssgout
					ldi		rmp,0xc0		;set pos line 2 start
					rcall	lcd_com4
					rcall	delay3s
					ldi		rmp,0x13
					sts		maxsteps,rmp
					ldi		rmp,0x88
					sts		maxsteps+1,rmp
					rjmp	speed0
speeddone:
			sts		maxsteps,rmp2
			sts		maxsteps+1,rmp	


revdir:		ldi		rmp,0x01		;clear display (1.64ms)
			rcall	lcd_com4
			rcall	delay200ms
			ldi		ZH,high(dirmssg<<1)
			ldi		ZL,low(dirmssg<<1)
			ldi		rmp,0x80		;set pos line 1 start
			rcall	lcd_com4
			rcall	delay50us
			rcall	mssgout

revdir2:	wdr
			ldi		rmp,0xc0		;set pos line 2 start
			rcall	lcd_com4
			rcall	delay50us

			lds		rmp,rotation
			tst		rmp
			brne	rd1
				ldi		rmp,'Y'
				rcall	lcd_data4
				rcall	delay50us
				ldi		rmp,'E'
				rcall	lcd_data4
				rcall	delay50us
				ldi		rmp,'S'
				rcall	lcd_data4
				rcall	delay50us
				rjmp	rd2
rd1:		ldi		rmp,'N'
				rcall	lcd_data4
				rcall	delay50us
				ldi		rmp,'O'
				rcall	lcd_data4
				rcall	delay50us
				ldi		rmp,' '
				rcall	lcd_data4
				rcall	delay50us
rd2:		

			rcall	checkeys	



			lds		rmp,keybuf
			cpi		rmp,0x05				;setup
			breq	rdone	

			lds		rmp,keybuf
			cpi		rmp,0x00				;any other key
			breq	revdir2	
				lds		rmp,rotation		;toggle value
				inc		rmp
				andi	rmp,0x01
				sts		rotation,rmp	
				clr		rmp
				sts		keybuf,rmp
				sbr		flags,0x02			;set update eeprom flag

			rjmp		revdir2

rdone:			clr		rmp
				sts		keybuf,rmp

			cbr		flags2,0x08				;motor direction flag
			lds		rmp,rotation
			sbrc	rmp,0
				sbr		flags2,0x08			;set


			ldi		rmp,0x01				;clear display (1.64ms)
			rcall	lcd_com4
			rcall	delay200ms


			sbrs	flags,1					;check values updated flag
				rjmp	setupend

				;calculate division ratio, steps*256/encoder counts
				lds		rmp,stepsrspindle
				mov		m16h,rmp
				lds		rmp,stepsrspindle+1
				mov		m16l,rmp
				clr		m16f

				lds		rmp2,stepsrencoder
				lds		rmp,stepsrencoder+1
				rcall	div2416u



				mov		rmp2,m16l			;division ratio (x256)
				mov		rmp,m16f

					
				or		r0,r1				;check if there is remainder
				breq	wholenum
					ldi		ZH,high(emssg3<<1)
					ldi		ZL,low(emssg3<<1)
					ldi		rmp,0x80		;set pos line 1 start
					rcall	lcd_com4
					rcall	delay50us
					rcall	mssgout
					ldi		ZH,high(emssg4<<1)
					ldi		ZL,low(emssg4<<1)
					ldi		rmp,0xc0		;set pos line 2 start
					rcall	lcd_com4
					rcall	delay50us
					rcall	mssgout
					rcall	delay3s

					ldi		ZH,high(emssg5<<1)
					ldi		ZL,low(emssg5<<1)
					ldi		rmp,0x80		;set pos line 1 start
					rcall	lcd_com4
					rcall	delay50us
					rcall	mssgout
					ldi		ZH,high(emssg6<<1)
					ldi		ZL,low(emssg6<<1)
					ldi		rmp,0xc0		;set pos line 2 start
					rcall	lcd_com4
					rcall	delay50us
					rcall	mssgout
					rcall	delay3s

					ldi		ZH,high(emssg7<<1)
					ldi		ZL,low(emssg7<<1)
					ldi		rmp,0x80		;set pos line 1 start
					rcall	lcd_com4
					rcall	delay50us
					rcall	mssgout
					ldi		ZH,high(emssg8<<1)
					ldi		ZL,low(emssg8<<1)
					ldi		rmp,0xc0		;set pos line 2 start
					rcall	lcd_com4
					rcall	delay50us
					rcall	mssgout
					rcall	delay3s

					ldi		ZH,high(emssg9<<1)
					ldi		ZL,low(emssg9<<1)
					ldi		rmp,0x80		;set pos line 1 start
					rcall	lcd_com4
					rcall	delay50us
					rcall	mssgout
					ldi		ZH,high(emssga<<1)
					ldi		ZL,low(emssga<<1)
					ldi		rmp,0xc0		;set pos line 2 start
					rcall	lcd_com4
					rcall	delay50us
					rcall	mssgout
					rcall	delay3s




wholenum:

			sts		divratio,m16l			;store division ratio
			sts		divratio+1,m16f




			rcall	save2eeprom
			cbr		flags,0x02

			rcall	calcacc			;update acc value

			ldi		rmp,0x80		;set pos line 1 
			rcall	lcd_com4
			rcall	delay50us
			ldi		ZH,high(udmssg1<<1)
			ldi		ZL,low(udmssg1<<1)
			rcall	mssgout
			ldi		rmp,0xc0		;set pos line 2 
			rcall	lcd_com4
			rcall	delay50us
			ldi		ZH,high(udmssg2<<1)
			ldi		ZL,low(udmssg2<<1)
			rcall	mssgout
			rcall	delay200ms
			ldi		rmp,0x08
			rcall	lcd_com4		;display off
			rcall	delay200ms
			rcall	delay200ms
			ldi		rmp,0x0c
			rcall	lcd_com4		;display on
			rcall	delay200ms

			ldi		rmp,0x01		;clear display (1.64ms)
			rcall	lcd_com4
			rcall	delay200ms
			rcall	delay200ms
setupend:
			ret


mssgout:					;put message (16 char max) on 1st line of screen 

mssgloop:
		lpm		rmp,z+				;load next character in message
		cpi		rmp,0xff			;escape char
		breq	mssgbail
		rcall	lcd_data4
		rcall	delay50us
		rjmp	mssgloop

mssgbail:
		ret

checkeys:
		sbrs	flags,3						;T0overflow (16ms)- done here to free ISR time for INTs
			rjmp	k0end
				cbr		flags,0x08			;clears interupt flag

				lds		rmp,ADCH			;read A2D value 
				sts		keyval,rmp			;store high byte
				lds		rmp,ADCSRA
				ori		rmp,0x40			;set start conversion bit
				sts		ADCSRA,rmp

				lds		rmp,keyval			;determine which key from A2D value
				;andi	rmp,0xf0			;use high nibble only for comp

k0a:			lds		rmp2,maxright
				cp		rmp,rmp2			;right
				brcc	k0b
					ldi		rmp,0x01		;=1
					sts		keyin,rmp
					rjmp	k01

k0b:			lds		rmp2,maxup
				cp		rmp,rmp2			;up
				brcc	k0c
					ldi		rmp,0x02		;=2
					sts		keyin,rmp
					rjmp	k01

k0c:			lds		rmp2,maxdown
				cp		rmp,rmp2			;down
				brcc	k0d
					ldi		rmp,0x03		;=3
					sts		keyin,rmp
					rjmp	k01



k0d:			lds		rmp2,maxleft
				cp		rmp,rmp2			;left
				brcc	k0f
					ldi		rmp,0x04		;=4
					sts		keyin,rmp
					rjmp	k01


k0f:			lds		rmp2,maxset
				cp		rmp,rmp2			;setup
				brcc	k0g
					ldi		rmp,0x05		;=5
					sts		keyin,rmp
					rjmp	k01


k0g:			clr		rmp					;no key pressed
				sts		keyin,rmp
				rjmp	k01c


					
k01:			lds		rmp2,keyold
				cp		rmp,rmp2			;compare to old value
				breq	k01a
					ldi		rmp2,0x3c		;new key pressed
					sts		keyheld,rmp2	;reload hold counter
					ldi		rmp2,0x01		
					sts		keydelay,rmp2	;set delay to minimum (key will be pushed into buffer)
					rjmp	k01b

k01a:			lds		rmp2,keyheld		;key held, decrease counter to zero
				dec		rmp2
				brmi	k01b				;don't store if negative
				sts		keyheld,rmp2					


k01b:			lds		rmp2,keydelay
				dec		rmp2
				sts		keydelay,rmp2
				brne	k01c
					sts		keybuf,rmp		;tranfer key to buffer (new or repeat)
					sbr		flags,0x01		;set key pressed flag
					ldi		rmp2,0x40		;~1s (long delay)
					sts		keydelay,rmp2
					lds		rmp2,keyheld	;check if held for more than limit
					tst		rmp2
					brne	k01c
						ldi		rmp2,0x06	; (short delay)
						sts		keydelay,rmp2


k01c:				
				sts		keyold,rmp			;save current key for next run	
k0end:			ret
		
null:
RESET:
 ldi r16, high(RAMEND)		; Main program start     
 out SPH,r16				; Set Stack Pointer to top of RAM
 ldi r16, low(RAMEND)     
 out SPL,r16

 ldi rmp,0b00100011			; 	 0=RS 1=E
 out DDRB,rmp
 

 ldi rmp,0b00110110	; 
 out DDRC,rmp
 clr	rmp
 out	PORTC,rmp

 ldi rmp,0b11110000			;   data bus for LCD1602 INT inputs 2,3
 out DDRD,rmp
 ldi	rmp,0b00001100		;pullups on INTs
 out	PORTD,rmp

 	ldi	rmp,0b01100000		;ADC ref vcc(5.0v),ADLAR= left adjust,x,nnnn = a2d input channel select 000=ADC0 (PC0)
	sts	ADMUX,rmp	
	ldi	rmp,0b10000111		;ADC enable, interupts off, prescale /128 @8mHz=62.5Khz (must be 50-200) 1 tick=16us, conversion=208us
	sts	ADCSRA,rmp
	ldi	rmp,0b00000001		;
	sts	DIDR0,rmp			;disable digital buffers on A2D pins, PC0

	ldi rmp,0b00000101		;INT0,1 interupt on change
	sts	EICRA,rmp
	ldi rmp,0b00000011		;enable INT interupt 
	out	EIMSK,rmp

							;Timer 0 clock
	clr	rmp
	out TCNT0,rmp
 	ldi	rmp,0b00000000		;normal counter mode, no compare outputs on ports
	out	TCCR0A,rmp			;normal counter mode
	ldi	rmp,0b00000101		;CS divide by 1024 = tick 64us, overflow 16.384ms (@16mHz)
	out	TCCR0B,rmp			;
	ldi	rmp,0x01
	sts	TIMSK0,rmp			;overflow interupt On 

	clr	rmp
	sts	TCCR1A,rmp			;Timer 1 -normal mode  TOV on 0xffff
	sts	TCCR1B,rmp			;no clock (stopped) ,ldi	0b00000010 =/8 (.05us tick)
	ldi	rmp,0xff
	sts	TCNT1H,rmp			;WRITE high byte first READ low byte first
	sts	TCNT1L,rmp
	ldi	rmp,0x01
	sts	TIMSK1,rmp			;overflow int ON

	clr	rmp					;Timer 2 clock
	sts TCCR2A,rmp			;normal counter mode, no compare outputs on ports
	ldi	rmp,0b00000111		;CS divide by 1024 = tock 64us, overflow 16.384ms (@16mHz)
	sts	TCCR2B,rmp			;
	ldi	rmp,0x01
	sts	TIMSK2,rmp			;overflow interupt On 


	 wdr					;set watchdog timeout
	lds rmp, WDTCSR
	ori r16, 0b00011000		;WDCE=1 WDE=1  enable changing (4 cycles max.)
	sts WDTCSR, rmp
	ldi rmp,0b00001110		;WDCE=1, WDE=1 watchdog on, 101 =.5s 110=1s
	sts WDTCSR, rmp
	

			;initialise variables
	clr		flags
	clr		flags2
	clr		step
	clr		encoderl
	clr		encoderf
	clr		encoderlold
	clr		mdtally
	ldi		rmp,startsteps
	mov		T2reload,rmp

		ldi	rmp,0x80			;clear ram area used for  variables 
	mov	YL,rmp
	ldi	yH,0x01
	clr	rmp
clrram:
	dec	YL	
	st	Y,rmp
	cpi	YL,0
	brne	clrram


	ldi		rmp,0x14			;here so unaffected if lock lost
	mov		maindiv,rmp

	
	




	rcall	copy2ram			;load eeprom values	
	lds		rmp,checksum
	cpi		rmp,0x41
	brne	loaddefaults
	lds		rmp,checksum+1
	cpi		rmp,0x42
	breq	eepromchecked

loaddefaults:					;eeprom is uninitialised, load default values
	ldi		rmp,0x41
	sts		checksum,rmp
	ldi		rmp,0x42
	sts		checksum+1,rmp

	ldi		rmp,0x02
	sts		stepsrencoder,rmp
	ldi		rmp,0x58
	sts		stepsrencoder+1,rmp

	ldi		rmp,0x02
	sts		stepsrspindle,rmp
	ldi		rmp,0x58
	sts		stepsrspindle+1,rmp

	ldi		rmp,0x13
	sts		acceleration,rmp
	ldi		rmp,0x88
	sts		acceleration+1,rmp

	ldi		rmp,0x03
	sts		maxsteps,rmp
	ldi		rmp,0xe8
	sts		maxsteps+1,rmp

	ldi		rmp,0x01
	sts		divratio,rmp
	ldi		rmp,0x00
	sts		divratio+1,rmp

	ldi		rmp,0x01
	sts		rotation,rmp

	ldi		rmp,0xd0
	sts		maxset,rmp
	ldi		rmp,0x8f
	sts		maxleft,rmp	
	ldi		rmp,0x6f
	sts		maxdown,rmp
	ldi		rmp,0x2f
	sts		maxup,rmp
	ldi		rmp,0x0f	
	sts		maxright,rmp


eepromchecked:


			



	cbr		flags2,0x08			;motor direction flag
	lds		rmp,rotation
	sbrc	rmp,0
		sbr		flags2,0x08		;set


	rcall	calcacc
	rcall	calcrpm

	sei							;interupts on

				lds		rmp,ADCSRA
				ori		rmp,0x40			;set start conversion bit
				sts		ADCSRA,rmp

	cbi		PORTB,E
	cbi		PORTB,RS
	rcall	delay16ms			;powerup delay

	

;
	
								;initialise display 
		

		ldi		rmp,0x30
		rcall	lcd_com
		rcall	delay5ms		;timings from datasheet

		rcall	lcd_com
		rcall	delay5ms

		rcall	lcd_com
		rcall	delay2ms
	
		ldi		rmp,0x20		;function set 4 bit 
		rcall	lcd_com			;change interface to 4 bit, must use 4 bit addressing from here on.
		rcall	delay50us

		ldi		rmp,0x08		;display off
		rcall	lcd_com4
		rcall	delay50us

		ldi		rmp,0x01		;clear display (1.64ms)
		rcall	lcd_com4
		rcall	delay2ms



		ldi		rmp,0x06		;Entry mode/cursor shift
		rcall	lcd_com4
		rcall	delay50us

		ldi		rmp,0x0c		;Display on (cursor/blink)
		rcall	lcd_com4
		rcall	delay50us

			ldi		rmp,0x80		;set pos line 1 start
			rcall	lcd_com4
			rcall	delay50us
			ldi		ZH,high(mssgpwr<<1)
			ldi		ZL,low(mssgpwr<<1)
			rcall	mssgout
			ldi		rmp,0xc0		;set pos line 1 start
			rcall	lcd_com4
			rcall	delay50us
			ldi		ZH,high(mssgver<<1)
			ldi		ZL,low(mssgver<<1)
			rcall	mssgout
			rcall	delay200ms
			rcall	delay200ms
				lds		rmp,ADCH			;read A2D value, throw away 1st read
				lds		rmp,ADCSRA
				ori		rmp,0x40			;set start conversion bit
				sts		ADCSRA,rmp
			
			rcall	delay200ms
			ldi		rmp,0x01		;clear display (1.64ms)
			rcall	lcd_com4
			rcall	delay200ms

				lds		rmp,ADCH			;read A2D value 
				cpi		rmp,0xf0			;check for any key pressed (no key =0xff)
				brcc	restart			
					rcall	configbuts

	

restart:



					

						clr	rmp
						sts		keybuf,rmp			;take key out of buffer


main:
	wdr

				rcall	checkeys


/*						lds		rmp,keyval
						clr		rmp2
						rcall	bin2bcd
						rcall	dispnum
						rjmp	main*/

				lds		rmp,keybuf
				cpi		rmp,0x05		;setup
					brne	m1
						clr		rmp
						sts		keybuf,rmp			;take key out of buffer
						sbrs	flags,4				;don't enter setup if spindle moving
						rcall	setup

m1:		
		sbrc		flags,4			;moving/stopped flag, 1=moving
			rjmp	sc10
				ldi		rmp,startsteps
				mov		T2reload,rmp			;reset speed and reload val
				clr		rmp
				sts		speed,rmp
				ldi		rmp,0x50
				sts		speed+1,rmp		
			
			

sc10:		
			sbrs	flags,7				;check increase speed flag, 1= increase (alter T2 reload value)
				rjmp	m2
			cbr		flags,0x80			;clear flag
			lds		r1,speed
			lds		r0,speed+1
			


			lds		rmp2,maxsteps				;speed max value 
			lds		rmp,maxsteps+1
			sub		rmp,r0
			sbc		rmp2,r1
			brmi	sc12						;if N set speed>max and cannot be increased
				
			lds		rmp,acc				;value 1-48
			clr		rmp2				
			add		r0,rmp				;increase speed by acc
			adc		r1,rmp2
			sts		speed,r1
			sts		speed+1,r0

			ldi		rmp,0x3d			;recalculate T2 reload period= 255-(15625/speed)
			mov		m16h,rmp
			ldi		rmp,0x09
			mov		m16l,rmp			;dividend
			mov		rmp2,r1				;divisor
			mov		rmp,r0
			rcall	div16u
			com		m16l
			mov		T2reload,m16l
			rjmp	m2





sc12:		nop							;speed is max-20 to max 
			


m2:			cbi		PORTC,5				;LED off


			mov		rmp,step			;check if step >1 or <-1 and light LED
			tst		rmp
			brmi	m2a
				ldi		rmp2,0x02		;step is +
				sub		rmp,rmp2		;if carry not set, step >1
				brcs	m2end
					sbi		PORTC,5
					rjmp	m2end
m2a:		inc		rmp
			inc		rmp					;add 2, if still negative step was <-1
			brpl	m2end
				sbi		PORTC,5			;LED on
m2end:

			ldi		rmp,0xc0			;set pos line 2 start
			rcall	lcd_com4
			rcall	delay50us
			ldi		ZH,high(mssgratio<<1)
			ldi		ZL,low(mssgratio<<1)
			ldi		rmp,0xc0			;set pos line 1 start
			rcall	lcd_com4
			rcall	delay50us
			rcall	mssgout


					

					lds		rmp,keybuf
					cpi		rmp,0x02			;up
					brne	cgratio1
						inc		maindiv
						mov		rmp,maindiv		;limit to 255
						com		rmp
						brne	cgratio1a
							dec		maindiv
cgratio1a:
						clr		rmp
						sts		keybuf,rmp
cgratio1:				lds		rmp,keybuf
					cpi		rmp,0x03			;down
					brne	cgratio2
						dec		maindiv
						mov		rmp,maindiv
						cpi		rmp,0x01
						brne	cgratio1b		;limit to 2
							inc		maindiv
cgratio1b:				clr		rmp
						sts		keybuf,rmp

cgratio2:
			mov		rmp,maindiv
			
			clr		rmp2

			rcall   bin2bcd

			ldi		rmp,0xcb		;set position
			rcall	lcd_com4
			rcall	delay50us

			mov		rmp2,BCDH
			andi	rmp2,0x0f
			ldi		rmp,0x30
			add		rmp,rmp2
			rcall	lcd_data4
			rcall	delay50us

			mov		rmp2,BCDL
			swap	rmp2
			andi	rmp2,0x0f
			ldi		rmp,0x30
			add		rmp,rmp2
			rcall	lcd_data4
			rcall	delay50us

			mov		rmp2,BCDL
			andi		rmp2,0x0f
			ldi		rmp,0x30
			add		rmp,rmp2
			rcall	lcd_data4
			rcall	delay50us

m3:
			sbrs	flags,2			;step overflow flag
				rjmp	m4
			clr		rmp
			sts		TIMSK2,rmp		;stepping off
			cbi		PORTC,5			;LED off

m3loop:		ldi		rmp,0x80		;set pos line 1 start
			rcall	lcd_com4
			rcall	delay50us
			ldi		ZH,high(emssgl1<<1)
			ldi		ZL,low(emssgl1<<1)
			rcall	mssgout
			rcall	delay50us

			ldi		rmp,0xc0		;set pos line 2 start
			rcall	lcd_com4
			rcall	delay50us
			ldi		ZH,high(emssgl2<<1)
			ldi		ZL,low(emssgl2<<1)
			rcall	mssgout
			rcall	delay50us

			rcall	delay3sk
			lds		rmp,keybuf
			tst		rmp
			brne	m3a

			ldi		rmp,0x80		;set pos line 1 start
			rcall	lcd_com4
			rcall	delay50us
			ldi		ZH,high(emssgl3<<1)
			ldi		ZL,low(emssgl3<<1)
			rcall	mssgout
			rcall	delay50us

			ldi		rmp,0xc0		;set pos line 2 start
			rcall	lcd_com4
			rcall	delay50us
			ldi		ZH,high(emssgl4<<1)
			ldi		ZL,low(emssgl4<<1)
			rcall	mssgout
			rcall	delay50us

			rcall	delay3sk
	
			lds		rmp,keybuf
			tst		rmp
			brne	m3a

			rjmp	m3loop

m3a:		clr		step
			clr		mdtally
			cbr		flags,0x04			;clear step overflow flag
			ldi		rmp,startsteps
			mov		T2reload,rmp
			ldi		rmp,0x01
			sts		TIMSK2,rmp			;stepping on
			clr		rmp
			sts		keybuf,rmp			;clear key buffer
			rjmp	restart
				
m4:			sbrs	flags2,1			;ready flag
				rjmp	m5
			cbr		flags2,0x02			;clear ready flag
			ldi		rmp,0x80			;set pos line 1 start
			rcall	lcd_com4
			rcall	delay50us
			ldi		ZH,high(mssgrpm<<1)
			ldi		ZL,low(mssgrpm<<1)
			rcall	mssgout
			rcall	delay50us



			ldi		YH,high(rpm+15)
			ldi		YL,low(rpm+15)
			ldi		XH,high(rpm+17)
			ldi		XL,low(rpm+17)
			ldi		counter,0x0f
shifttable:
			ld		rmp,-Y	
			st		-X,rmp
			dec		counter
			brne	shifttable

			lds		rmp,rpmpre
			lds		rmp2,rpmmult
			mul		rmp,rmp2
			sts		rpm,r1				;store most recent value
			sts		rpm+1,r0

			ldi		YH,high(rpm)		;average (8)
			ldi		YL,low(rpm)
			clr		rmp
			clr		rmp2
			ldi		counter,0x08
averpm:		ld		r1,Y+
			ld		r0,Y+
			add		rmp,r0				;add 8 times
			adc		rmp2,r1
			dec		counter
			brne	averpm

			lsr		rmp2				;/8
			ror		rmp
			lsr		rmp2				
			ror		rmp
			lsr		rmp2				
			ror		rmp

			rcall	bin2bcd

			lds		rmp,rpmdispctr
			dec		rmp
			sts		rpmdispctr,rmp
			brmi	m4a
				rjmp	m5
m4a:		ldi		rmp,0x10		;reload interval counter (approx 4 per second)
			sts		rpmdispctr,rmp
			ldi		rmp,0x84		;set pos line 1 start
			rcall	lcd_com4
			rcall	delay50us

			sbrs	flags2,0		;overflow flag
			rcall	dispnumfree
			sbrs	flags2,0		;overflow flag
			rjmp	m5
				ldi		rmp,'-'
				rcall	lcd_data4
				rcall	delay50us
				ldi		rmp,'-'
				rcall	lcd_data4
				rcall	delay50us
				ldi		rmp,'-'
				rcall	lcd_data4
				rcall	delay50us
				ldi		rmp,'-'
				rcall	lcd_data4
				rcall	delay50us
m5:
			clr	rmp
			sts		keybuf,rmp			;take any unused key out of buffer

			rjmp	main


mssgpwr:	.db	"POWER UP--RESET ",0xff,0
mssgver:	.db "Version 1.1     ",0xff,0
stepenc:	.db	"ENCODER CPR     ",0xff,0
stepspin:	.db	"Steps/r SPINDLE ",0xff,0
accmssg:	.db	"Acceleration    ",0xff,0
accmssg2:	.db	"     Steps/S^2  ",0xff,0
acc10mssg:	.db	"Limit           ",0xff,0
spmssg:		.db	"Stepper maximum ",0xff,0
spmssg2:	.db "     steps/sec  ",0xff,0
dirmssg:	.db	"Reverse rotation",0xff,0
ratio:		.db	"Ratio 1:        ",0xff,0
mssgrpm:	.db "RPM:",0xff,0
udmssg1:	.db	" Configuration  ",0xff,0
udmssg2:	.db	"    Updated     ",0xff,0
emssg1:		.db	"ERROR:-must have",0xff,0
emssg2:		.db	"Encoder>=Spindle",0xff,0
emssg3:		.db	"  **WARNING!**  ",0xff,0
emssg4:		.db	" Rounding Error ",0xff,0
emssg5:		.db	"Try a different ",0xff,0
emssg6:		.db	"ratio.          ",0xff,0
emssg7:		.db	"Steps x 64      ",0xff,0
emssg8:		.db	0xfd," CPR...        ",0xff,0
emssg9:		.db	"must be a whole ",0xff,0
emssga:		.db	"number.         ",0xff,0

emssgl1:	.db	"ERROR-Lost lock.",0xff,0
emssgl2:	.db	"Increase...     ",0xff,0
emssgl3:	.db	"acceleration or ",0xff,0
emssgl4:	.db	"decrease speed. ",0xff,0
emssg15:	.db "ERROR- must be  ",0xff,0
emssg16:	.db ">= 100 steps/sec",0xff,0
emssg17:	.db "< 5000 steps/sec",0xff,0

mssgdiv:	.db	"Division Ratio: ",0xff,0
mssgratio:	.db "Divide by:",0xff,0

mssgcb1:	.db	"   Configure    ",0xff,0
mssgcb2:	.db	"    Buttons     ",0xff,0
mssgpress:	.db	"Press:          ",0xff,0
mssgset:	.db	"      *SET*     ",0xff,0
mssgl:		.db	"      *LEFT*    ",0xff,0
mssgd:		.db	"      *DOWN*    ",0xff,0
mssgu:		.db	"      *UP*      ",0xff,0
mssgr:		.db	"      *RIGHT*   ",0xff,0
