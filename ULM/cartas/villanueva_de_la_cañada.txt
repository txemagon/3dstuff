Tel.: 91 815 65 96 / 616 706 179.

Para venir volando, estos son nuestros datos:
Coordenadas: 40º 26' 12'' N 04º 01' 30'' O
Elevación: 652 m
Frecuencia de radio: 130.125
Pistas: 03/21 de 270 x 40 m (inclinación 2%).
12/30 de 150 x 15 m (solo emergencias).
Superficie: Tierra compactada.
Tráficos: Por el este. Altura 400 ft 
