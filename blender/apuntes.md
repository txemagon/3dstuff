# Apuntes de Blender

## Configuración Inicial

Barra espaciadora: => Buscar (Preferences > keymap)  

Interface: 1.25 (Aumentar tamaño de letra)

Preferences > Themes > 3D View > Vertex Size
                               > Wire edit

Interface > Line width: Thick

### Pasar Blender a mm
 
 Scene Properties 
                  > Unit Scale: 0.001
                  > length: mm
               
 Overlay Options
        > scale: 0.001
        
 Clipping Point.  
   View
        > End: add 0
        > Lock camera to view
        
   Camera Settings
        > End: add 0
        

Botón derecho on the taskbar: => Scene Info.

### Addons

1. Bool tool (cutting away)
1. Loop tool
1. 3D print toolbox
1. PDT (Precission Drawing Tools)
1. Extra Objects: Add mesh
1. Outline To SVG download: https://gumroad.com/l/O2SVG​ - Outline To SVG Webpage: https://www.makertales.com/blender-outline-to-svg
1. MeasureIt: https://docs.blender.org/manual/en/dev/addons/3d_view/measureit.html
1. Exact Edit: https://blenderartists.org/t/exact-edit-set-exact-distances-or-angle-measures-between-objects/700238/19
1. Align Tools
1. Oscurart Tools
1. CAD Transform: Download: bit.ly/CADmoveBlender - https://www.youtube.com/watch?v=vhm_b-YVdK4&t=0s - 

Save Preferences
 
## Manejo

### Vista

 Pan:   Shift+Middle
 Zoom:  Wheel, Ctl+Middle
 Rotate: Middle
 
 0: Camera  
 1: 
 7: Top View, click z
 
 .: Enfocar el objeto. View> Frame selected.
 
 /: Isolate
 
 Tab: Switch object, edit mode
 
 Alt+z: X Ray
 
 Q: Quick Menu
 
 c: Circle selection
 
#### Overlay

Edge distances 

Edit > Preferences > Themes > 3D View > Edge Length text : Change color.

Face Orientation
Display normals (size)


#### Measure Tool


DRAG+Ctrl: Snap
 
### Selección

W: Tweak Tool -> Select Box
 
 click, drag
 shift
 control
 
 a: Select all
 Shft+click: Edge Loop Selection
 
 ctrl++ Wide selection
 ctrl+- Narrow selection
 
 h: Hide an object
 Alt+h:  Show all objects
 Shft+h: Isolate Object
 Ctrl+h: Hide collections
 
### Objeto
 
 scale: s
 rotate: r
 g: move
 
 Shft+d: Duplicar


Ctrl+3: Subdivisión con 3 niveles (Subdivision Modifier).
Right click: Shade smooth


 
### Edición
 
 Vertex: 1
 edge: 2
 surface: 3
 
 x: Borrar

 Shft+a: Menú Añadir
 e: extrude

F: Fill. Fill between vertices (edge) or edges (surfaces).

Menú Contextual:

Merge Vertices > By Distance

Ctrl+r: Loop

alt+click: Select loop

GG: Edge slide

M: Merge vertices

Y: Disconnect face

#### Constraints:

##### Axis

x, y, z: Restrict to access
Shft+{x,y,z}: Restrict to plane.

Double Hit: Go to local Coordinate System

##### Distances

Type distance.

##### Snapping

Ctrl+move

Vertex Snapping
Absolute grid


#### Origins

3D cursors

Shift+Right click: Move 3D cursor

Shft+s > Cursor to world origin (snapping menu)
Shft+c: Cursor to origin (center scene)

Menu Contextual > Set Origin:

1. Geometry to Origin
1. Origin to Geometry


## Addons

### PDT

#### PDT Design Tools

Join Verts

Fillet [Radius & Segments (even number) ]

Move To (3D Cursor) > Delta (from the selected entity)
                            (from current position)
                            
Move To > Arc center (select 3 points)
                            


#### PDT Design Operations

Set a vertex:

Do at %: 50
Working plane: xy
Move 3d Cursor


Intersect two lines:

Intersect

### MeasureIt

View > MeasureIt Tools

Vertex mode

Segment + Show

Change size an color for each label

Configuration (change color and font for all labels)

Items > Change to milimeters

### Exact Edit

Select 2 vertices
Set measure

To set Length
 Set: Free form dot + anchor dot
 Set measure
 
To set scale

### Align

Items > Align Tools

Mind who the active object is.

### Oscurart

Distribute objects

### CAD Transform

Activate CAD > Tool

E > Snap to edges

Shft+f: Middle of face
v: vertex

### Mesh Tool

To offset edges


### Tiny CAD Mesh tools

VTX Intersection



## Organization

[View Layer]

Filter > Restriction Toggles > {Selectable, Disable in Viewports}

F2: Rename Object

### Collections

Shft+G: Select Grouped > collection

### Joins

Crl J: Join 2 Objects

P: Part (selection, material, loose parts)

### Parenting

Drag on top on layer view: Ctrl to link, shft to parent

Ctl+p: Parent
Alt+p: unparent

Shft+a: Empty (arrow)

### Materials

Material > Viewport Display > color
Ctrl+L:


## 3D

Ctrl+A: Apply menu
Shft N: Mesh: Recalculate Normals.


## Extrusión


E: Extrude
Middle click: remove constraint in extrusion.
Twice an axis (x, y, z) remove constraints.

Alt+e: Extrusion Options
Ctrl+-: Cut Operation
Ctrl+Shft+-: Cut and delete the cutting object
Ctrl+Shft++: Unite



Shft N: Mesh: Recalculate Normals.

## Bool Tool

Cutting Object + cutted => Ctrl+-
Ctrl+Shft+-: Cut and delete the cutting object

Ctrl+Shft++: Unite

Ctrl+Shft+*: Intersect

Ctrl+Shft+/: Slice

## Duplication

Shft+D: Hard duplication
Alt+d: Linked duplication

Unlink: Object > relations > Make a single User > Object & Data (Alt+u)

Ctrl+L: Link Object Data




